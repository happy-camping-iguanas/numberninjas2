﻿<%@ Page Title="About" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.vb" Inherits="NumberNinjas2.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p> Welcome to Number Ninjas! </p>
    <p> We appreciate you trusting us to lead you to becoming a number ninja!</p>
    
</asp:Content>
