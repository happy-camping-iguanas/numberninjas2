﻿<%@ Page Title="Contact" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.vb" Inherits="NumberNinjas2.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
    <p>Contact The Team</p>
    <p> </p>
    <p>Audrey Chaffin</p>
    <a href="mailto:chaffina@etsu.edu">chaffina@etsu.edu</a>
    <p> </p>
    <p>Zachary Carrier</p>
    <a href="mailto:carrierz@etsu.edu">carrierz@etsu.edu</a>
    <p> </p>
    <p>Manan Shah</p>
    <a href="mailto:shahm1@etsu.edu">shahm1@etsu.edu</a>
    <p> </p>
    <p>Rodney Bowles</p>
    <a href="mailto:bowlesr@etsu.edu">bowlesr@etsu.edu</a>
   
</asp:Content>
